#!/bin/bash

project_dir=script.ir.blaster
if [ $# -ne 1 ]; then
    printf "Usage: $0 hostname\n"
    exit 1
fi

rm -rf dist
mkdir -p dist
cp -r $project_dir dist/
rm -rf dist/${project_dir}/.git
rm -f dist/${project_dir}/.travis.yml
cd dist && \
    # rm -rf ${project_dir}/.git && \
    zip -r ir-blaster.zip $project_dir && \
    scp ir-blaster.zip root@${1}:~ && \
    cd ..
